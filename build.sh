#!/bin/bash
git clone https://github.com/arendst/Sonoff-Tasmota.git
cd Sonoff-Tasmota/
git checkout v6.4.1

cp ../user_config_override.h sonoff/
export PLATFORMIO_BUILD_FLAGS=-DUSE_CONFIG_OVERRIDE

platformio run -e sonoff
